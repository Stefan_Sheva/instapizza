var passport = require('passport'),
    mongoose = require('mongoose');

module.exports = function() {
    var Admin = mongoose.model('Admin');

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        Admin.findOne(
            {_id: id},
            '-password',
            function(err, user) {
                done(err, user);
            }
        );
    });

    require('./local.js')();
};
