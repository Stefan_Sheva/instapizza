var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PizzaScheme = new Schema({
	// TODO: Add admin authorization
	name: String,
	description: String,
	photoUrl: String,
	sizes: [{
		label: String,
		price: Number
	}]
});

var Pizza = mongoose.model('Pizza', PizzaScheme);
module.exports = Pizza;
