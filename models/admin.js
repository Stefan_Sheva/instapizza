var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var Schema = mongoose.Schema;

var AdminSchema = new Schema({
    // TODO: Add Photo
    name: String,
    username: String,
    password: String,
    pizzas: [{
        name: String,
        description: String,
        photoUrl: String,
        sizes: [{
            label: String,
            price: Number
        }]
    }],
    location: {
        lat: String,
        lng: String
    }
});

AdminSchema.plugin(passportLocalMongoose);
var Admin = mongoose.model('Admin', AdminSchema);
module.exports = Admin;
