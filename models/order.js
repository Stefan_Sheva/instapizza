var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
	adminId: String,
	pizzas: [{
		name: String,
		size: String
	}],
	orderAddress: String,
	buyerName: String,
	buyerPhone: String,
	orderNotes: String,
	total: String,
	lng: String,
	lat: String,
	time: {
		type: Date,
		default: null
	}
});

var Order = mongoose.model('Order', OrderSchema);
module.exports = Order;
