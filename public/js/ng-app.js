var app = angular.module('PizzaApp', ['ngRoute', 'ngAnimate', 'angularUtils.directives.dirPagination']);

app.controller('NavController', function($scope, $rootScope, $http, $location, CheckAuth) {
    $rootScope.loggedIn = false;
    $rootScope.username = '';

    CheckAuth.CheckAuth().then(function(response) {
        $scope.username = response.data;
        $rootScope.loggedIn = true;
    }, function(error) {
        console.log("Check auth error: " + error);
    });

    $rootScope.$on('userLoggedIn', function(event, username) {
        $rootScope.loggedIn = true;
        $rootScope.username = username;
    });

    $scope.logout = function() {
        $http({
                method: 'GET',
                url: '/logout',
            })
            .success(function(data) {
                if (data === "success") {
                    $location.path('/');
                    $rootScope.loggedIn = false;
                } else {
                    console.log(err);
                }
            })
            .error(function(err) {
                console.log(err);
            });
    }
});

app.controller('UserInfoController', function($scope, $rootScope, $http, $location, $timeout) {
    $scope.cart = $rootScope.cart;
    $scope.cartTotal = $rootScope.cartTotal;
    $scope.order = null;
    $scope.user = {};
    $scope.user.lat = 41.99592123113886;
    $scope.user.lng = 21.431465715169907;
    $scope.restaurantId = $rootScope.restaurantId;

    $scope.removeFromCart = function(index) {
        $scope.cartTotal -= $scope.cart[index].size.price;
        $scope.cart.splice(index, 1);
    }

    $scope.placeOrder = function() {
        var orderData = {
            "pizzas": $scope.cart,
            "user": $scope.user,
            "total": $scope.cartTotal,
            "restaurantId": $scope.restaurantId
        };

        $http({
            method: 'POST',
            url: '/placeOrder',
            data: $.param(orderData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(data) {
            $scope.order = data;
            $("#orderSuccessModal").modal();
        }).error(function(err) {
            console.log(err);
        });
    }

    $scope.redirect = function(dest) {
        $timeout(function() {
            $location.path(dest);
        }, 500);
    }
});

app.controller('RestaurantsController', function($scope, $rootScope, $location, GetRestaurantsFactory) {
    $scope.restaurants = null;

    GetRestaurantsFactory.GetRestaurants().then(function(response) {
        $scope.restaurants = response.data;
    }, function(error) {
        console.log(error);
    });

    $scope.selectRestaurant = function(restaurantId, restaurantName) {
        $rootScope.selectedRestaurantName = restaurantName;
        $location.path('/restaurants/' + restaurantId);
    }
});

app.controller('OrderController', function($scope, $rootScope, $location, $timeout, $routeParams, GetPizzasFactory) {
    $scope.restaurantId = $routeParams.id;
    $scope.restaurantName = $rootScope.selectedRestaurantName;
    $scope.pizzas = null;

    GetPizzasFactory.GetPizzasMethod($scope.restaurantId).then(function(response) {
        $scope.pizzas = response.data;
    }, function(err) {
        console.log(err);
    });

    $scope.cart = [];
    $scope.cartTotal = 0;
    $scope.selectedId = 0;
    $scope.selectedSize = 0;
    $scope.selectedPrice = 0;

    $scope.addToCart = function(pizza, $event) {
        $scope.cartTotal += $scope.selectedPrice;
        var chosenPizza = {
            _id: pizza._id,
            name: pizza.name,
            description: pizza.description,
            photoUrl: pizza.photoUrl,
            size: {
                label: $scope.selectedSize,
                price: $scope.selectedPrice
            }
        }
        $scope.cart.push(chosenPizza);
        $event.currentTarget.innerText = "Успешно!";
        $timeout(function() {
            $event.currentTarget.innerText = "Сакам!";
        }, 1000);
    }

    $scope.removeFromCart = function(index) {
        var selectedPizza = $scope.cart[index];
        $scope.cartTotal -= selectedPizza.size.price;
        $scope.cart.splice(index, 1);
    }

    $scope.selectPizza = function(id, size, price) {
        $scope.selectedId = 0;
        $scope.selectedSize = 0;
        $scope.selectedPrice = 0;

        $scope.selectedPrice += parseFloat(price);
        $scope.selectedId = id;
        $scope.selectedSize = size;
    }

    $scope.finishOrder = function() {
        $rootScope.cart = $scope.cart;
        $rootScope.cartTotal = $scope.cartTotal;
        $rootScope.restaurantId = $scope.restaurantId;
        $location.path('/userInfo');
    }
});

app.controller('AuthController', function($scope, $http, $location, $rootScope) {

    if ($rootScope.loggedIn) {
        $location.path('/admin');
    }

    $scope.formData = {};
    $scope.formData.location = {
        "lat": "41.99592123113886",
        "lng": "21.431465715169907"
    };
    $scope.error = false;
    $scope.errorMessage = "";
    $scope.user = {};

    $scope.login = function() {
        $http({
                method: 'POST',
                url: '/login',
                data: $.param($scope.formData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function(data) {
                $rootScope.$emit('userLoggedIn', data.username);
                $location.path('/admin');
            })
            .error(function(err) {
                $scope.error = true;
                $scope.errorMessage = "Wrong username or password!";
            });
    }

    $scope.register = function() {
        $scope.error = false;
        $scope.errorMessage = "";
        if ($scope.formData.password === $scope.formData.confirmPassword) {
            $http({
                    method: 'POST',
                    url: '/register',
                    data: $.param($scope.formData),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data) {
                    if (data.name.indexOf("Error") > -1) {
                        $scope.error = true;
                        $scope.errorMessage = data.message;
                    }
                })
                .error(function(err) {
                    console.log(err);
                    $scope.error = true;
                    $scope.errorMessage = err.message;
                });
        } else {
            $scope.error = true;
            $scope.errorMessage = "Passwords don't match!";
        }
    }
});

app.controller('AdminController', ['$scope', '$http', 'GetPizzasFactory', 'GetOrdersFactory', function($scope, $http, GetPizzasFactory, GetOrdersFactory) {
    $scope.orders = false;
    $scope.pizzas = true;

    $scope.search = "";

    $scope.ordersCollection = [];
    $scope.pizzaCollection = [];

    GetPizzasFactory.GetMyPizzas().then(function(response) {
        $scope.pizzaCollection = response;
    }, function(error) {
        console.log(error);
    });
    GetOrdersFactory.GetOrders().then(function(response) {
        $scope.ordersCollection = response.data;
    }, function(error) {
        console.log(error);
    });

    var submitPizzaData = new FormData();
    var editPizzaData = new FormData();

    $scope.submitPizza = {};
    $scope.editPizza = {};

    $scope.submitPizza.photo = null;
    $scope.submitPizza.pizzaName = "";
    $scope.submitPizza.pizzaDescription = "";
    $scope.submitPizza.smallPrice = 0;
    $scope.submitPizza.mediumPrice = 0;
    $scope.submitPizza.largePrice = 0;

    $scope.showOrders = function() {
        $scope.orders = true;
        $scope.pizzas = false;
    }

    $scope.showPizzas = function() {
        $scope.orders = false;
        $scope.pizzas = true;
    }

    $scope.uploadPhoto = function($files) {
        angular.forEach($files, function(value, key) {
            submitPizzaData.append('photo', value);
        });
    }

    $scope.uploadNewPhoto = function($files) {
        angular.forEach($files, function(value, key) {
            editPizzaData.append('photo', value);
        });
    }

    $scope.submitPizza = function() {
        submitPizzaData.append('pizzaName', $scope.submitPizza.pizzaName);
        submitPizzaData.append('pizzaDescription', $scope.submitPizza.pizzaDescription);
        submitPizzaData.append('smallPrice', $scope.submitPizza.smallPrice);
        submitPizzaData.append('mediumPrice', $scope.submitPizza.mediumPrice);
        submitPizzaData.append('largePrice', $scope.submitPizza.largePrice);

        $http({
                method: 'POST',
                url: '/submitPizza',
                data: submitPizzaData,
                headers: {
                    'Content-Type': undefined
                }
            })
            .success(function(data) {
                submitPizzaData = new FormData();
                GetPizzasFactory.GetMyPizzas().then(function(response) {
                    $scope.pizzaCollection = response;
                }, function(error) {
                    console.log(error);
                });
            })
            .error(function(err) {
                console.log(err);
            });
    }

    $scope.updatePizza = function() {
        editPizzaData.append('pizzaName', $scope.editPizza.pizzaName);
        editPizzaData.append('pizzaDescription', $scope.editPizza.pizzaDescription);
        editPizzaData.append('smallPrice', $scope.editPizza.smallPrice);
        editPizzaData.append('mediumPrice', $scope.editPizza.mediumPrice);
        editPizzaData.append('largePrice', $scope.editPizza.largePrice);
        editPizzaData.append('id', $scope.editPizza.id);

        $http({
                method: 'POST',
                url: '/updatePizza',
                data: editPizzaData,
                headers: {
                    'Content-Type': undefined
                }
            })
            .success(function(data) {
                editPizzaData = new FormData();
                GetPizzasFactory.GetMyPizzas().then(function(response) {
                    $scope.pizzaCollection = response;
                    document.getElementById("closeModal").click();
                }, function(error) {
                    console.log(error);
                });
            })
            .error(function(err) {
                console.log(err);
            });
    }

    $scope.setupUpdatePizza = function(pizza) {
        $scope.editPizza.pizzaName = pizza.name;
        $scope.editPizza.pizzaDescription = pizza.description;
        $scope.editPizza.smallPrice = pizza.sizes[0].price;
        $scope.editPizza.mediumPrice = pizza.sizes[1].price;
        $scope.editPizza.largePrice = pizza.sizes[2].price;
        $scope.editPizza.photoUrl = pizza.photoUrl;
        $scope.editPizza.id = '';
        $scope.editPizza.id = pizza._id;
    }

    $scope.removePizza = function(id) {
        $scope.formData = {};
        $scope.formData.id = id;

        $http({
                method: 'POST',
                url: '/deletePizza',
                data: $.param($scope.formData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .success(function(data) {
                GetPizzasFactory.GetMyPizzas().then(function(response) {
                    $scope.pizzaCollection = response;
                }, function(error) {
                    console.log(error);
                });
            })
            .error(function(err) {
                console.log(err);
            });
    }
}]);

app.factory('GetPizzasFactory', function($q, $http) {
    return {
        GetMyPizzas: function() {
            var deferred = $q.defer(),
                httpPromise = $http.get('/menu');

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log(error);
            });
            return deferred.promise;
        },
        GetPizzasMethod: function(id) {
            var deferred = $q.defer(),
                httpPromise = $http.get('/menu/' + id);

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log(error);
            });
            return deferred.promise;
        }
    };
});

app.factory('GetOrdersFactory', function($q, $http) {
    return {
        GetOrders: function() {
            var deferred = $q.defer(),
                httpPromise = $http.get('/getOrders');

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log(error);
            });
            return deferred.promise;
        }
    };
});

app.factory('GetRestaurantsFactory', function($q, $http) {
    return {
        GetRestaurants: function() {
            var deferred = $q.defer(),
                httpPromise = $http.get('/getAdmins');

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log(error);
            });
            return deferred.promise;
        }
    }
});

app.factory('CheckAuth', function($q, $http) {
    return {
        CheckAuth: function() {
            var deferred = $q.defer(),
                httpPromise = $http.get('/checkAuth');

            httpPromise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                //console.log(error);
            });
            return deferred.promise;
        }
    };
});

app.directive('ngFiles', ['$parse', function($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function(event) {
            onChange(scope, {
                $files: event.target.files
            });
        });
    };
    return {
        link: fn_link
    }
}])

app.config(function($routeProvider, $locationProvider, paginationTemplateProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/select-restaurant.html',
            controller: 'RestaurantsController'
        })
        .when('/restaurants/:id', {
            templateUrl: 'views/restaurant.html',
            controller: 'OrderController'
        })
        .when('/userInfo', {
            templateUrl: 'views/complete-order.html',
            controller: 'UserInfoController'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'AuthController'
        })
        .when('/register', {
            templateUrl: 'views/register.html',
            controller: 'AuthController'
        })
        .when('/admin', {
            templateUrl: 'views/panel.html',
            controller: 'AdminController',
            access: {
                requiredLogin: true
            }
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    paginationTemplateProvider.setPath('../dirPagination.tpl.html');
});

app.run(function($rootScope, $location) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (!$rootScope.loggedIn) {
            if (next.templateUrl === "views/panel.html") {
                $location.path("/login");
            }
        }
    });
});
