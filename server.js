var express = require('express');
var app = express();
var mongoose = require('mongoose');
var multer = require('multer');
var storageMulter = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'uploads/images/')
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
})
var upload = multer({
    storage: storageMulter
})
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var Admin = require('./models/admin');
var Pizza = require('./models/pizza');
var Order = require('./models/order');

// Drop database on connect
mongoose.connect('mongodb://localhost/pizzadb', function(){
  mongoose.connection.db.dropDatabase();
})

mongoose.connect('mongodb://localhost/pizzadb')

app.use(cookieParser());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies
// app.use(session({ secret: 'keyboard cat', cookie: { maxAge: 60000 }}))
app.use(require('express-session')({
    secret: '20bukbJnGvup2YnYkf0V',
    resave: false,
    saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));

passport.use(new LocalStrategy(Admin.authenticate()));
passport.serializeUser(Admin.serializeUser());
passport.deserializeUser(Admin.deserializeUser());

app.get('/', function(req, res) {
    res.sendFile('index');
});

app.get('/checkAuth', checkAuthenticated, function(req, res) {
    res.status(200).send(req.user.username);
});

app.post('/placeOrder', function(req, res) {
    var restaurantId = req.body.restaurantId;
    var pizzasResponse = req.body.pizzas;
    var pizzasArray = [];
    var timeNow = new Date();
    for (var i = 0; i < pizzasResponse.length; i++) {
        var pizz = {
            name: pizzasResponse[i].name,
            size: pizzasResponse[i].size.label
        }
        pizzasArray.push(pizz);
    }
    var newOrder = new Order({
        adminId: restaurantId,
        pizzas: pizzasArray,
        orderAddress: req.body.user.address,
        buyerName: req.body.user.name,
        buyerPhone: req.body.user.phone,
        orderNotes: req.body.user.notes,
        total: req.body.total,
        lng: req.body.user.lng,
        lat: req.body.user.lat,
        time: timeNow
    });

    newOrder.save(function(err, result) {
        if (err) throw err;
        res.status(200).send(result);
    });
});

app.get('/getOrders', checkAuthenticated, function(req, res) {
    Order.find({
        adminId: req.user._id
    }, function(err, result) {
        if (err) res.send(err);
        res.status(200).send(result);
    });
});

app.get('/getAdmins', function(req, res) {
    Admin.find({}, function(err, result) {
        if (err) res.send(err);
        res.status(200).send(result);
    });
});

app.post('/register', function(req, res) {
    Admin.register(new Admin({
        name: req.body.name,
        username: req.body.email,
        location: {
            lat: req.body.location.lat,
            lng: req.body.location.lng
        }
    }), req.body.password, function(err, account) {
        if (err) {
            res.status(401).send(err);
        } else {
            passport.authenticate('local')(req, res, function() {
                res.status(200).send(account);
            });
        }
    });
});

app.get('/deleteAccount', isLoggedIn, function(req, res) {

});

app.post('/submitPizza', isLoggedIn, upload.single('photo'), function(req, res) {
    var newPizza = {
        name: req.body.pizzaName,
        description: req.body.pizzaDescription,
        photoUrl: req.file.path,
        sizes: [{
            label: 'мала',
            price: req.body.smallPrice
        }, {
            label: 'средна',
            price: req.body.mediumPrice
        }, {
            label: 'голема',
            price: req.body.largePrice
        }]
    }

    Admin.findByIdAndUpdate(
        req.user._id, {
            $push: {
                pizzas: newPizza
            }
        }, {
            safe: true,
            upsert: true
        },
        function(err, model) {
            if (err != null) {
                console.log("ERROR! " + err);
            } else {
                res.status(200).send(newPizza);
            }
        }
    );
});

app.post('/updatePizza', isLoggedIn, upload.single('photo'), function(req, res) {
    var pizzaId = req.body.id;
    var adminId = req.user._id;
    if (typeof req.file === 'undefined') {
        Admin.update({
            "_id": adminId,
            "pizzas._id": pizzaId
        }, {
            $set: {
                "pizzas.$.name": req.body.pizzaName,
                "pizzas.$.description": req.body.pizzaDescription,
                "pizzas.$.sizes": [{
                    "label": 'мала',
                    "price": req.body.smallPrice
                }, {
                    "label": 'средна',
                    "price": req.body.mediumPrice
                }, {
                    "label": 'голема',
                    "price": req.body.largePrice
                }]
            }
        }, function(err, numAffected) {
            if (err) {
                console.log("Error while updating pizza: " + err);
                res.status(400).send(err);
            } else {
                res.status(200).send("Success");
            }
        });
    } else {
        Admin.update({
            "_id": adminId,
            "pizzas._id": pizzaId
        }, {
            $set: {
                "pizzas.$.name": req.body.pizzaName,
                "pizzas.$.description": req.body.pizzaDescription,
                "pizzas.$.photoUrl": req.file.path,
                "pizzas.$.sizes": [{
                    "label": 'мала',
                    "price": req.body.smallPrice
                }, {
                    "label": 'средна',
                    "price": req.body.mediumPrice
                }, {
                    "label": 'голема',
                    "price": req.body.largePrice
                }]
            }
        }, function(err, numAffected) {
            if (err) {
                console.log("Error while updating pizza: " + err);
                res.status(400).send(err);
            } else {
                res.status(200).send("Success");
            }
        });
    }
});

app.post('/deletePizza', isLoggedIn, function(req, res) {
    // Pizza.remove({
    // 	_id: req.body.id
    // }, function(err) {
    // 	if (err) res.send(err);
    // 	res.status(200).send('success');
    // });
    Admin.findById(req.user._id, function(err, admin) {
        admin.pizzas.forEach(function(pizza, index, array) {
            if (pizza._id == req.body.id) {
                console.log("MATCH FOUND!");
                Admin.findByIdAndUpdate(
                    req.user._id, {
                        $pull: {
                            pizzas: {
                                _id: req.body.id
                            }
                        }
                    }, {
                        safe: true,
                        upsert: true
                    },
                    function(err, model) {
                        if (err != null) {
                            res.status(401).send(err);
                        } else {
                            res.status(200).send('success');
                        }
                    }
                );
            }
        });
    });
});

app.get('/menu', function(req, res) {
    if (req.user != null) {
        if (req.user._id != null) {
            // Get all pizzas
            Admin.findById(req.user._id, function(err, admin) {
                res.send(admin.pizzas);
            });
        }
    } else {
        res.status(401).send();
    }
});
app.get('/menu/:id', function(req, res) {
    // Get all pizzas
    Admin.findById(req.params.id, function(err, admin) {
        res.send(admin.pizzas);
    });
});

app.get('/logout', function(req, res) {
    req.logout();
    res.send('success');
});

app.post('/login', passport.authenticate('local'), function(req, res) {
    if (req.session) {
        req.session.user = req.user;
    }
    res.status(200).send(req.user);
});

app.listen(8080);
console.log('Server listening on http://localhost:8080');

function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.redirect('/');
}

function checkAuthenticated(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.status(401).send('error');
}
